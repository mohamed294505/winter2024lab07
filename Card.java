public class Card{
	private String suit;
	private int value;
	
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	public int getValue(){
		return this.value;
	}
	
	public String toString() {
		return value + " of " + suit + "\n";
    }
	
	public double calculateScore(){
		double playerScore = value;
		
		if(suit.equals("Hearts")){
			playerScore += 0.4;
		}
		if(suit.equals("Spades")){
			playerScore += 0.3;
		}
		if(suit.equals("Diamonds")){
			playerScore += 0.2;
		}
		if(suit.equals("Clubs")){
			playerScore += 0.1;
		}
		
		return playerScore;
	}
}