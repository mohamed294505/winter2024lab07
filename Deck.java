import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	private String[] suits;
	private int[] values;
	
	public Deck(){
		suits = new String[]{"Hearts", "Spades", "Diamonds", "Clubs"};
		values = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
		this.rng = new Random();
		cards = new Card[52];
		
		int count = 0;
		for(String suit : suits){
			for(int value : values){
				cards[count] = new Card(suit, value);
				count ++;
			}
		}
		numberOfCards = count;
	}
	
	public int length(){
		return numberOfCards;
	}
		
    public Card drawTopCard() {
        if (numberOfCards > 0) {
            Card topCard = cards[numberOfCards - 1];
            numberOfCards--; 
            return topCard;
        } else {
            System.out.println("Deck is empty.");
            return null;
        }
    }
	
	 public String toString() {
        String result = "";
        for (int i = 0; i < numberOfCards; i++) {
            result += cards[i].toString() + "\n";
        }
        return result;
    }
	
	public void shuffle(){
		for(int i = 0; i < this.cards.length; i++){
			int position = rng.nextInt(numberOfCards);
			Card placeHolder = this.cards[i];
			this.cards[i] = this.cards[position];
			this.cards[position] = placeHolder;
		}
	}
}