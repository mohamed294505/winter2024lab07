import java.util.Scanner;

public class SimpleWar {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Deck deck = new Deck();

        deck.shuffle();
		
		int player1Points = 0;
		int player2Points = 0;
		
		while(deck.length() > 0){
			Card player1Card = deck.drawTopCard();
			Card player2Card = deck.drawTopCard();
			
			double p1Score = player1Card.calculateScore();
			double p2Score = player2Card.calculateScore();
			
			System.out.print("Player 1 Card is: " + player1Card + "and their score is: " + p1Score + "\n");
			System.out.print("Player 2 Card is: " + player2Card + "and their score is: " + p2Score + "\n");
			
			String winner = "";
			if(p1Score > p2Score){
				player1Points++;
			} else{
				player2Points++;
			}
		}
		
		if(player1Points > player2Points){
			System.out.print("Congratulations! The Winner Is Player 1!!"); 
		} else{
			System.out.print("Congratulations! The Winner Is Player 2!!");
		}
		
    }
}